var pInfo = {
	id: 'com.googlecode.droidcare.providers.labas',
	name: 'l-a-b-a-s',
	version: '0.1',
	description: 'TODO',
	updateFunc: update
};

dCare.registerProvider(pInfo);

function update(provider) {
	var httpService = Context.httpService;
	var userAccount = Context.userAccount;
	dCare.http.addParam('data[username]', userAccount.user.login);
	dCare.http.addParam('data[password]', userAccount.user.password);
	dCare.http.addParam('data[ref]', '');
	dCare.http.post('https://mano.labas.lt/lt/selfcare?auth');
	var response = dCare.http.get('https://mano.labas.lt/lt/selfcare/');
	var document = response.dom;
	var e = document.select(".login").iterator().next();
	var likutis = e.getElementsContainingOwnText("Lt").text();
	var galioja = e.getElementsContainingOwnText("galioja").select("b");
	var numerisGaliojaIki = galioja.get(0).text();
	var saskaitaGaliojaIki = galioja.get(1).text();
	var model = provider.model;
	model.put('likutis', likutis);
	model.put('numerisGalioja', numerisGaliojaIki);
	model.put('saskaitaGalioja', saskaitaGaliojaIki);
	userAccount.user.left = likutis;
	userAccount.user.validTo = saskaitaGaliojaIki;
	userAccount.user.numbValidTo = numerisGaliojaIki;
}