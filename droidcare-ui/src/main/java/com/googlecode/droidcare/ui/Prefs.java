package com.googlecode.droidcare.ui;

/*
 * #%L
 * droidcare-ui
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.googlecode.droidcare.common.AppContext;
import com.googlecode.droidcare.common.provider.SelfServiceProvider;

import android.content.Context;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class Prefs extends PreferenceActivity {
	
	private static final String PROVIDER_KEY = "provider";
	private static final String LOGIN_KEY = "login" ;	
	private static final String PASS_KEY = "pass" ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
		ListPreference listPreference = (ListPreference) findPreference(PROVIDER_KEY);
		listPreference.setEntryValues(getProviderIds());
		listPreference.setEntries(getProviderValues());
	}
	
	private String[] getProviderIds() {
		Set<String> ids = AppContext.getInstance().getProviderManager().getProviderRegistry().keySet();
		String[] arr = new String[ids.size()];
		return ids.toArray(arr);
	}
	
	private String[] getProviderValues() {
		List<String> valuesList = new ArrayList<String>();
		Collection<SelfServiceProvider> providerList = AppContext.getInstance().getProviderManager().getProviderRegistry().values();
		String[] arr = new String[providerList.size()];
		for (SelfServiceProvider provider : providerList) {
			valuesList.add(provider.getProviderDescriptor().getName());
		}
		return valuesList.toArray(arr);
	}
	
	public static String getLoginFromSettings(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getString(LOGIN_KEY, "");
	}
	
	public static String getPassFromSettings(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getString(PASS_KEY, "");
	}
	
	public static String getProviderFromSettings(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getString(PROVIDER_KEY, "");
	}
}
