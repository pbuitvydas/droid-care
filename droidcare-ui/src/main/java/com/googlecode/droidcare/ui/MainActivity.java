package com.googlecode.droidcare.ui;

/*
 * #%L
 * droidcare-ui
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.InputStream;
import java.util.Map.Entry;

import com.google.inject.Guice;
import com.googlecode.droidcare.common.AppContext;
import com.googlecode.droidcare.common.ScriptEngine;
import com.googlecode.droidcare.common.di.GuiceModule;
import com.googlecode.droidcare.common.provider.ProviderUpdateEvent;
import com.googlecode.droidcare.common.provider.SelfServiceProvider;
import com.googlecode.droidcare.common.provider.ProviderListener;
import com.googlecode.droidcare.common.provider.ProviderManager;
import com.googlecode.droidcare.common.usr.User;
import com.googlecode.droidcare.common.usr.UserAccount;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements ProviderListener {
	
	private TextView tvLikutis;
	private TextView tvSaskaitaGalioja;
	private TextView tvNumerisGalioja;
	private TextView tvHtml;
	static ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Guice.createInjector(new GuiceModule());
        setContentView(R.layout.activity_main);
        tvLikutis = (TextView) findViewById(R.id.tvLikutis);
        tvHtml = (TextView) findViewById(R.id.tvHtml);
        //http://javatechig.com/android/display-html-in-android-textview/
        //tvHtml.setText(Html.fromHtml("<div align=\"center\">Labas</div>"), TextView.BufferType.SPANNABLE);
        tvSaskaitaGalioja = (TextView) findViewById(R.id.tvSaskaitaGaliojaIki);
        tvNumerisGalioja = (TextView) findViewById(R.id.tvNumerisGaliojaIki);
        dialog = new ProgressDialog(this);
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	AppContext.getInstance().getProviderManager().loadBuiltInProviders();
    }
    
    public void updateUserLogin() {
    	UserAccount userAccount = AppContext.getInstance().getUserAccount();
    	userAccount.getUser().setLogin(Prefs.getLoginFromSettings(this));
    	userAccount.getUser().setPassword(Prefs.getPassFromSettings(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    public void update(View v) {
    	updateUserLogin();
    	String selectedProviderId = Prefs.getProviderFromSettings(this);
    	if (!validate(selectedProviderId)) {
    		return;
    	}
    	showUpateDialog();
    	SelfServiceProvider p = AppContext.getInstance().getProviderManager().get(selectedProviderId);
    	p.updateAsync(this);
    }
    
    public void showUpateDialog() {
    	dialog.setMessage("Atnaujinama...");
    	dialog.setIndeterminate(true);
    	dialog.setCancelable(false);
    	dialog.show();
    }
    
    public void hideUpdatedialog() {
		dialog.hide();
    }
    
    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) 
          getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    } 
    
    public boolean validate(String providerId) {
    	UserAccount usrA = AppContext.getInstance().getUserAccount();
    	boolean valid = true;
    	if (isEmpty(usrA.getUser().getLogin())) {
    		valid = false;
    		Toast.makeText(MainActivity.this, "Nenurodytas prisijungimo vardas", Toast.LENGTH_SHORT).show();
    	}
    	if (isEmpty(usrA.getUser().getPassword())) {
    		valid = false;
    		Toast.makeText(MainActivity.this, "Nenurodytas prisijungimo slaptazodis", Toast.LENGTH_SHORT).show();
    	}
    	if (isEmpty(providerId)) {
    		valid = false;
    		Toast.makeText(MainActivity.this, "Nepasirinktas rysio tiekejas", Toast.LENGTH_SHORT).show();
    	}
    	if (!isNetworkAvailable()) {
    		Toast.makeText(this, "Nera interneto rysio", Toast.LENGTH_SHORT).show();
    		valid = false;
    	}
    	return valid;
    }
    
    private boolean isEmpty(String str) {
    	if (str == null) {
    		return true;
    	}
    	if (str.length() == 0) {
    		return true;
    	}
    	return false;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			startActivity(new Intent(this, Prefs.class));
			return true;
		case R.id.menu_exit:
			finish();
			return true;
		}
		return false;
    }
    
    public void afterUpdate(SelfServiceProvider p) {
		hideUpdatedialog();
	    Toast.makeText(MainActivity.this, "Atnaujinta sekmingai", Toast.LENGTH_SHORT).show();
    	User usr = AppContext.getInstance().getUserAccount().getUser();
    	tvLikutis.setText(usr.getLeft());
    	tvSaskaitaGalioja.setText(usr.getValidTo());
    	tvNumerisGalioja.setText(usr.getNumbValidTo());
    }

	@Override
	public void onUpdateComplete(final ProviderUpdateEvent e) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				afterUpdate(e.getProvider());
			}
		});
	}
}
