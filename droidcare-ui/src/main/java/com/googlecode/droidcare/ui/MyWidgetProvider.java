package com.googlecode.droidcare.ui;

/*
 * #%L
 * droidcare-ui
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.widget.RemoteViews;

import com.googlecode.droidcare.common.AppContext;
import com.googlecode.droidcare.common.provider.ProviderListener;
import com.googlecode.droidcare.common.provider.ProviderUpdateEvent;
import com.googlecode.droidcare.common.provider.SelfServiceProvider;
import com.googlecode.droidcare.common.usr.UserAccount;

public class MyWidgetProvider extends AppWidgetProvider implements
		ProviderListener {
	private static final Logger log = LoggerFactory.getLogger(MyWidgetProvider.class);
	private static final String ACTION_CLICK = "ACTION_CLICK";
	
	private RemoteViews remoteViews;
	private AppWidgetManager wManager;
	private int[] appWidgetIds;
	private Context ctx;

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {

		doTheThing(context);
		this.appWidgetIds = appWidgetIds;
		wManager = appWidgetManager;
		ctx = context;
//		ComponentName thisWidget = new ComponentName(context,
//				MyWidgetProvider.class);
//		int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
//		for (int widgetId : allWidgetIds) {
//			// create some random data
//			int number = (new Random().nextInt(100));
//
//			remoteViews = new RemoteViews(context.getPackageName(),
//					R.layout.widget_layout);
//			Log.w("WidgetExample", String.valueOf(number));
//			// Set the text
//			remoteViews.setTextViewText(R.id.content, String.valueOf(number));
//
//			// Register an onClickListener
//			Intent intent = new Intent(context, MyWidgetProvider.class);
//
//			intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
//			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
//
//			PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
//					0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//			remoteViews.setOnClickPendingIntent(R.id.content, pendingIntent);
//			appWidgetManager.updateAppWidget(widgetId, remoteViews);
//		}
	}

	public void doTheThing(Context ctx) {
		loadProviders();
		updateUserLogin(ctx);
		String selectedProviderId = Prefs.getProviderFromSettings(ctx);
		SelfServiceProvider p = getProvider(selectedProviderId);
		UpdateTask t = new UpdateTask();
		t.execute(p);
		
	}

	public void loadProviders() {
		AppContext.getInstance().getProviderManager().loadBuiltInProviders();
	}

	public SelfServiceProvider getProvider(String id) {
		return AppContext.getInstance().getProviderManager().get(id);
	}

	public void updateUserLogin(Context ctx) {
		UserAccount userAccount = AppContext.getInstance().getUserAccount();
		userAccount.getUser().setLogin(Prefs.getLoginFromSettings(ctx));
		userAccount.getUser().setPassword(Prefs.getPassFromSettings(ctx));
	}

	public void doComplete(SelfServiceProvider p) {

	}

	@Override
	public void onUpdateComplete(ProviderUpdateEvent e) {

	}
	
	public class UpdateTask extends AsyncTask<SelfServiceProvider, Void, String> {

		@Override
		protected String doInBackground(SelfServiceProvider... params) {
			for (SelfServiceProvider p : params) {
				try {
					p.update();
					return AppContext.getInstance().getViewRenderer().renderView(p);	
				} catch (Throwable e) {
					log.error("Caught error", e);
				}
			}
			return "<font color=\"red\">Įvyko klaida</font>";
		}
		
		@Override
		protected void onPostExecute(String result) {
			for (int widgetId : appWidgetIds) {
				RemoteViews w = new RemoteViews(ctx.getPackageName(), R.layout.widget_layout);
				w.setTextViewText(R.id.content, Html.fromHtml(result));
			}
			log.info("Updated");
		}
		
	}
}
