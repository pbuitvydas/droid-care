/*
 * #%L
 * droidcare-providers
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
var pInfo = {
	id: 'com.googlecode.droidcare.providers.labas',
	name: 'l-a-b-a-s',
	version: '0.2',
	description: 'TODO',
	updateFunc: update,
	view: 'com/googlecode/droidcare/providers/labas/view.xml'
};

dCare.registerProvider(pInfo);

function update(provider) {
	var httpService = Context.httpService;
	var userAccount = Context.userAccount;
	dCare.http.addParam('data[username]', userAccount.user.login);
	dCare.http.addParam('data[password]', userAccount.user.password);
	dCare.http.addParam('data[ref]', '');
	dCare.http.post('https://mano.labas.lt/lt/selfcare?auth');
	var response = dCare.http.get('https://mano.labas.lt/lt/selfcare/');
	var document = response.dom;
	var e = document.select(".login").iterator().next();
	var likutis = e.getElementsContainingOwnText("Lt").text();
	var galioja = e.getElementsContainingOwnText("galioja").select("b");
	var numerisGaliojaIki = galioja.get(0).text();
	var saskaitaGaliojaIki = galioja.get(1).text();
	var model = provider.model;
	model.put('likutis', likutis);
	model.put('numerisGalioja', numerisGaliojaIki);
	model.put('saskaitaGalioja', saskaitaGaliojaIki);
	userAccount.user.left = likutis;
	userAccount.user.validTo = saskaitaGaliojaIki;
	userAccount.user.numbValidTo = numerisGaliojaIki;
}
