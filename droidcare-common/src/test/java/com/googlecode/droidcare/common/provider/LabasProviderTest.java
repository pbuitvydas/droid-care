package com.googlecode.droidcare.common.provider;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.googlecode.droidcare.common.AppContext;
import com.googlecode.droidcare.common.DroidCareUnitTest;
import com.googlecode.droidcare.common.usr.UserAccount;

@Ignore
public class LabasProviderTest extends DroidCareUnitTest {
	private static boolean cont = true;
	
	
	@Test
	public void testProvider() throws InterruptedException {
		UserAccount ua = AppContext.getInstance().getUserAccount();
		ua.getUser().setLogin("863844218");
		ua.getUser().setPassword("vst86PUJA");
		LabasProvider l = new LabasProvider();
		l.updateAsync(new ProviderListener() {
			@Override
			public void onUpdateComplete(ProviderUpdateEvent e) {
				cont = false;				
			}
		});
		Thread.sleep(1000 * 20);
		assertFalse(cont);
	}

}
