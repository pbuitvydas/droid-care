package com.googlecode.droidcare.common;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

 
import org.junit.BeforeClass;

import com.google.inject.Guice;
import com.googlecode.droidcare.common.api.net.HttpService;
import com.googlecode.droidcare.common.di.GuiceModule;
import com.googlecode.droidcare.common.provider.ProviderManager;
import com.googlecode.droidcare.common.usr.UserAccount;
import com.googlecode.droidcare.common.view.ViewRenderer;

public class DroidCareUnitTest {
	
	protected ScriptEngine scriptEngine = AppContext.getInstance().getScriptEngine();
	protected ProviderManager providerManager = AppContext.getInstance().getProviderManager();
	protected UserAccount userAccount = AppContext.getInstance().getUserAccount();
	protected HttpService httpService = AppContext.getInstance().getHttpService();
	protected ViewRenderer viewRenderer = AppContext.getInstance().getViewRenderer();
	
	@BeforeClass
	public static void beforeClass() {
		Guice.createInjector(new GuiceModule());
	}

}
