package com.googlecode.droidcare.common;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;

import com.googlecode.droidcare.common.model.Model;
import com.googlecode.droidcare.common.provider.SelfServiceProvider;
import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;



public class JavascriptTest extends DroidCareUnitTest {
	
	@Test
	public void test() {
		userAccount.getUser().setLogin("Login");
		userAccount.getUser().setPassword("superSeecret");
		scriptEngine.eval(getClass().getResourceAsStream("controller.js"));
		SelfServiceProvider p = providerManager.getProviderRegistry().get("wikiProvider");
		p.update();
		generateHtml(p.getModel());
	}
	
	public void generateHtml(Model model) {
		InputStream is = getClass().getResourceAsStream("view.html");
		viewRenderer.loadTemplate(is);
		System.out.println(viewRenderer.renderTemplate(model));
	}

}
