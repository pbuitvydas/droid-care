package com.googlecode.droidcare.common.js;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.junit.Assert.*;
import org.junit.Test;

import com.googlecode.droidcare.common.AppContext;
import com.googlecode.droidcare.common.DroidCareUnitTest;
import com.googlecode.droidcare.common.impl.exception.JsError;

public class JavascriptApiTest extends DroidCareUnitTest {
	
	@Test
	public void testRootObject() {
		assertTrue((Boolean) eval("dCare != null"));
	}
	
	@Test
	public void testLoggers() {
		assertTrue((Boolean) eval("dCare.logInfo != null"));
		assertTrue((Boolean) eval("dCare.logDebug != null"));
		assertTrue((Boolean) eval("dCare.logError != null"));
		
		eval("dCare.logInfo('Info message')");
		eval("dCare.logInfo('Info message {}', 'param1')");
		eval("dCare.logInfo('Info message {} {}', 'param1', 'param2')");
		
		eval("dCare.logDebug('Debug message')");
		eval("dCare.logDebug('Debug message {}', 'param1')");
		eval("dCare.logDebug('Debug message {} {}', 'param1', 'param2')");
		
		eval("dCare.logError('Error message')");
		eval("dCare.logError('Error message {}', 'param1')");
		eval("dCare.logError('Error message {} {}', 'param1', 'param2')");
		
	}
	
	@Test
	public void testHttp() {
		assertTrue((Boolean) eval("dCare.http != null"));
		assertTrue((Boolean) eval("dCare.http.addParam != null"));
		assertTrue((Boolean) eval("dCare.http.get != null"));
		assertTrue((Boolean) eval("dCare.http.post != null"));
		
		assertNotNull(eval("dCare.http.get('http://www.wikipedia.org/')"));
		assertNotNull(eval("dCare.http.post('http://www.wikipedia.org/')"));
	}
	
	@Test
	public void providerRegistration() {
		assertTrue((Boolean) eval("dCare.registerProvider != null"));
		
		eval("dCare.registerProvider({id: 'testProvider', updateFunc: dCare.registerProvider})");
		assertNotNull(AppContext.getInstance().getProviderManager().get("testProvider"));
		
	}
	
	@Test(expected = JsError.class)
	public void testErrorManager() {
		assertTrue((Boolean) eval("dCare.addError != null"));
		eval("dCare.addError('oops it happens')");
	}
	
	public Object eval(String script) {
		return scriptEngine.eval(script);
	}

}
