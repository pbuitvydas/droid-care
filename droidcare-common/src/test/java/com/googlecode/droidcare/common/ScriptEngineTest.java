package com.googlecode.droidcare.common;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mozilla.javascript.Function;

public class ScriptEngineTest extends DroidCareUnitTest {
	
	public static final String SCRIP_FILE = "testScript.js";
	
	@BeforeClass
	public static void createScriptFile() throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(SCRIP_FILE);
		pw.println("'' + 2 * 2");
		pw.close();
	}
	
	@AfterClass
	public static void deleteScriptFile() {
		File f = new File(SCRIP_FILE);
		if (f.exists()) {
			f.delete();
		}
	}

	@Test
	public void testLoadApi() throws Exception {
		assertTrue((Boolean) scriptEngine.eval("dCare != null")); 
	}

	@Test
	public void testEvalString() throws Exception {
		String str = (String) scriptEngine.eval("'' + 2 * 2");
		assertEquals("4", str);
	}

	@Test
	public void testEvalFile() throws Exception {
		String str = (String) scriptEngine.eval(new File(SCRIP_FILE));
		assertEquals("4", str);
	}

	@Test
	public void testEvalInputStream() throws Exception {
		FileInputStream is = new FileInputStream(SCRIP_FILE);
		String str = (String) scriptEngine.eval(is);
		assertEquals("4", str);
		is.close();
	}

	@Test
	public void testCallFunctionStringObjectArray() throws Exception {
		scriptEngine.eval("function test(a, b) {return '' + a * b;}");
		String str = (String) scriptEngine.callFunction("test", 2, 2);
		assertEquals("4", str);
	}

	@Test
	public void testCallFunctionFunctionObjectArray() throws Exception {
		Function f = (Function) scriptEngine.eval("function test(a, b) {return '' + a * b;}");
		String str = (String) scriptEngine.callFunction(f, 2, 2);
		assertEquals("4", str);
	}

}
