/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
var pInfo = {
	id: 'wikiProvider',
	name: 'wikiProvider',
	version: '0.1',
	description: '',
	updateFunc: update
};

dCare.registerProvider(pInfo);

function update(provider) {
	var httpService = Context.httpService;
	var userAccount = Context.userAccount;
	dCare.logInfo('have username {}', userAccount.user.login);
	dCare.logInfo('have password {}', userAccount.user.password);
	var response = dCare.http.get('http://www.wikipedia.org/');
	var div = response.dom.getElementsByAttributeValue("lang", "en").iterator().next();
	dCare.logInfo(div.text());
	var articles = div.getElementsByTag("small").text();
	provider.model.put('articles', articles);
}
