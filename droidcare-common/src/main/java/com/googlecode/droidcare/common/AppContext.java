package com.googlecode.droidcare.common;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.googlecode.droidcare.common.api.exception.ExceptionHandler;
import com.googlecode.droidcare.common.api.net.HttpService;
import com.googlecode.droidcare.common.di.GuiceModule;
import com.googlecode.droidcare.common.js.JsErrorManager;
import com.googlecode.droidcare.common.js.JsLogger;
import com.googlecode.droidcare.common.provider.ProviderManager;
import com.googlecode.droidcare.common.provider.SelfServiceProvider;
import com.googlecode.droidcare.common.usr.UserAccount;
import com.googlecode.droidcare.common.view.ViewRenderer;

@Singleton
@SuppressWarnings("rawtypes")
public final class AppContext {
	
	private ProviderManager providerManager;
	private ScriptEngine scriptEngine;
	private HttpService httpService;
	private UserAccount userAccount;
	private JsLogger logger;
	private JsErrorManager errorManager;
	private ViewRenderer viewRenderer;
	private Injector injector;
	
	@Inject
	protected static Provider<AppContext> contexProvider;
	
	@Inject
	protected static Provider<SelfServiceProvider> selfServiceProvider;
	
	@Inject
	protected static Provider<ExceptionHandler> exceptionHandlerProvider;
	
	public ProviderManager getProviderManager() {
		return providerManager;
	}

	@Inject
	public void setProviderManager(ProviderManager providerManager) {
		this.providerManager = providerManager;
	}


	public ScriptEngine getScriptEngine() {
		return scriptEngine;
	}

	@Inject
	public void setScriptEngine(ScriptEngine scriptEngine) {
		this.scriptEngine = scriptEngine;
	}


	public HttpService getHttpService() {
		return httpService;
	}

	@Inject
	public void setHttpService(HttpService httpService) {
		this.httpService = httpService;
	}


	public UserAccount getUserAccount() {
		return userAccount;
	}

	@Inject
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}


	public Injector getInjector() {
		return injector;
	}

	@Inject
	public void setInjector(Injector injector) {
		this.injector = injector;
	}

	public JsLogger getLogger() {
		return logger;
	}

	@Inject
	public void setLogger(JsLogger logger) {
		this.logger = logger;
	}

	public JsErrorManager getErrorManager() {
		return errorManager;
	}

	@Inject
	public void setErrorManager(JsErrorManager errorManager) {
		this.errorManager = errorManager;
	}

	public ViewRenderer getViewRenderer() {
		return viewRenderer;
	}

	@Inject
	public void setViewRenderer(ViewRenderer viewRenderer) {
		this.viewRenderer = viewRenderer;
	}

	public SelfServiceProvider newProvider() {
		return selfServiceProvider.get();
	}
	
	public static AppContext getInstance() {
		if (contexProvider == null) {
			bootstrap();
		}
		return contexProvider.get();
	}
	
	public static ExceptionHandler getExceptionHandler() {
		return exceptionHandlerProvider.get();
	}
	
	public static void bootstrap() {
		Guice.createInjector(new GuiceModule());
	}

}
