package com.googlecode.droidcare.common.provider;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.googlecode.droidcare.common.AppContext;
import com.googlecode.droidcare.common.api.net.HttpService;
import com.googlecode.droidcare.common.net.Response;

public class OmnitelProvider extends SelfServiceProvider {
	
	@Override
	@Deprecated
	public void init() {
	}
	
	@Override
	public void update() {
		HttpService http = AppContext.getInstance().getHttpService();
		http.addParam("IDToken1", "66252199");
		http.addParam("IDToken2", "vst86PUJA");
		http.addParam("login", "Prisijungti");
		Response r = http.post("https://savitarna.omnitel.lt/8CFE8BJEaHR0cDovL21vcHNydi5vbW5pdGVsLmx0OjgwODAvb21uaWxvZ2luL3dhaXQvbG9naW4uZG8=OnRTdcw8");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		r = http.post("https://savitarna.omnitel.lt/redirect/8CFE8BJEaHR0cDovL21vcHNydi5vbW5pdGVsLmx0OjgwODA=OnRTdcw8/omnilogin/login.do");
		r = http.get("https://savitarna.omnitel.lt/8CFE8BJEaHR0cDovL21vcHNydi5vbW5pdGVsLmx0OjgwODAvcG9ydGFsL2R0P2FjdGlvbj1jb250ZW50JnByb3ZpZGVyPU9tbmlQb3J0YWw=OnRTdcw8");
		r = http.get("https://savitarna.omnitel.lt/8CFE8BJEaHR0cDovL21vcHNydi5vbW5pdGVsLmx0OjgwODAvcG9ydGFsL2R0P3Byb3ZpZGVyPU9tbmlQb3J0YWwmYWN0aW9uPXByb2Nlc3MmdGFiPW9tbmlfdG9wX2FjY291bnRzX251bWJlcmN1cnJlbnRiaWxsOnRTdcw8");
		System.out.println(r.getDom().html());
	}

}
