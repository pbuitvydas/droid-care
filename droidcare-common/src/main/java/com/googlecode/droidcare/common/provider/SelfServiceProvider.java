package com.googlecode.droidcare.common.provider;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.google.inject.Inject;
import com.googlecode.droidcare.common.api.provider.ProviderService;
import com.googlecode.droidcare.common.model.Model;

public class SelfServiceProvider {
	
	private ProviderDescriptor providerDescriptor;
	private ProviderService providerService;
	private Model model;
	
	@Deprecated
	public void init() {
		providerService.init(this);
	}
	
	public void update() {
		providerService.update(this);
	}
	
	public void updateAsync(final ProviderListener listener) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				update();
				listener.onUpdateComplete(new ProviderUpdateEvent(SelfServiceProvider.this));
			}
		}).start();
	}

	public ProviderDescriptor getProviderDescriptor() {
		return providerDescriptor;
	}

	@Inject
	public void setProviderDescriptor(ProviderDescriptor providerDescriptor) {
		this.providerDescriptor = providerDescriptor;
	}

	public ProviderService getProviderService() {
		return providerService;
	}
	
	@Inject
	public void setProviderService(ProviderService providerService) {
		this.providerService = providerService;
	}

	public Model getModel() {
		return model;
	}
	
	@Inject
	public void setModel(Model model) {
		this.model = model;
	}

}
