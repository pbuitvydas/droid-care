package com.googlecode.droidcare.common.provider;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.googlecode.droidcare.common.AppContext;
import com.googlecode.droidcare.common.ScriptEngine;
import com.googlecode.droidcare.providers.BuiltinProviders;


public class ProviderManager {

	private Map<String, SelfServiceProvider> providerRegistry = new HashMap<String, SelfServiceProvider>();
	
	public void registerProvider(String providerId, SelfServiceProvider provider) {
		providerRegistry.put(providerId, provider);
	}
	
	public Map<String, SelfServiceProvider> getProviderRegistry() {
		return providerRegistry;
	}
	
	public SelfServiceProvider get(String providerId) {
		return providerRegistry.get(providerId);
	}
	
	public Collection<SelfServiceProvider> getProviders() {
		return providerRegistry.values();
	}
	
	public void loadBuiltInProviders() {
		ScriptEngine engine = AppContext.getInstance().getScriptEngine();
		for (String ctlr : BuiltinProviders.LIST) {
			engine.eval(ProviderManager.class.getClassLoader().getResourceAsStream(ctlr));
		}
	}

}
