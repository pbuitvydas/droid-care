package com.googlecode.droidcare.common.provider;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.googlecode.droidcare.common.AppContext;
import com.googlecode.droidcare.common.api.net.HttpService;
import com.googlecode.droidcare.common.net.Response;
import com.googlecode.droidcare.common.usr.UserAccount;

public class LabasProvider extends SelfServiceProvider {
	
	private static final String PROVIDER_ID = "l-a-b-a-s";
	
	@Override
	public void init() {
		
	}
	
	@Override
	public void update() {
		HttpService s = AppContext.getInstance().getHttpService();
		UserAccount ua = AppContext.getInstance().getUserAccount();
		s.addParam("data[username]", ua.getUser().getLogin());
		s.addParam("data[password]", ua.getUser().getPassword());
		s.addParam("data[ref]", "");
		s.post("https://mano.labas.lt/lt/selfcare?auth");
		
		Response r = s.get("https://mano.labas.lt/lt/selfcare/");
		Element e = r.getDom().select(".login").iterator().next();
		String likuts = e.getElementsContainingOwnText("Lt").text();
		ua.getUser().setLeft(likuts);
		Elements galioja = e.getElementsContainingOwnText("galioja").select("b");
		String numerisGaliojaIki = galioja.get(0).text();
		ua.getUser().setNumbValidTo(numerisGaliojaIki);
		String saskaitaGaliojaIki = galioja.get(1).text();
		ua.getUser().setValidTo(saskaitaGaliojaIki);
	}
	
	@Override
	public void updateAsync(final ProviderListener listener) {
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				update();
				listener.onUpdateComplete(new ProviderUpdateEvent(LabasProvider.this));
			}
		});
		t.start();
	}

}
