package com.googlecode.droidcare.common.impl.provider;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.mozilla.javascript.Function;

import com.googlecode.droidcare.common.AppContext;
import com.googlecode.droidcare.common.ScriptEngine;
import com.googlecode.droidcare.common.api.provider.ProviderService;
import com.googlecode.droidcare.common.provider.SelfServiceProvider;

public class ProviderServiceJs implements ProviderService {
	
	private Function updateFunc;
	private Function initFunc;

	public void update(SelfServiceProvider provider) {
		ScriptEngine engine = AppContext.getInstance().getScriptEngine();
		engine.callFunction(updateFunc, provider);
	}
	
	@Deprecated
	public void init(SelfServiceProvider provider) {
		if (initFunc == null) return;
		ScriptEngine engine = AppContext.getInstance().getScriptEngine();
		engine.callFunction(initFunc, provider);
	}
	
	public Function getUpdateFunc() {
		return updateFunc;
	}

	public void setUpdateFunc(Function updateFunc) {
		this.updateFunc = updateFunc;
	}

	public Function getInitFunc() {
		return initFunc;
	}

	public void setInitFunc(Function initFunc) {
		this.initFunc = initFunc;
	}

}
