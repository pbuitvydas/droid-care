package com.googlecode.droidcare.common.di;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.googlecode.droidcare.common.AppContext;
import com.googlecode.droidcare.common.api.exception.ExceptionHandler;
import com.googlecode.droidcare.common.api.net.HttpService;
import com.googlecode.droidcare.common.api.provider.ProviderService;
import com.googlecode.droidcare.common.impl.exception.ExceptionHandlerBasic;
import com.googlecode.droidcare.common.impl.net.ApacheHttpService;
import com.googlecode.droidcare.common.impl.provider.ProviderServiceJs;

public class GuiceModule extends BaseModule {

	@Override
	protected void configure() {
		requestStaticInjection(AppContext.class);
		bind(ExceptionHandler.class).to(ExceptionHandlerBasic.class);
		bind(BaseModule.class).to(GuiceModule.class);
		bind(HttpService.class).to(ApacheHttpService.class);
		bind(ProviderService.class).to(ProviderServiceJs.class);
	}

}
