package com.googlecode.droidcare.common.usr;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


public class User {

	private String login;
	private String password;
	private String left;
	private String validTo;
	private String numbValidTo;

	public final String getLogin() {
		return login;
	}

	public final void setLogin(final String login) {
		this.login = login;
	}

	public final String getPassword() {
		return password;
	}

	public final void setPassword(final String password) {
		this.password = password;
	}

	public final String getLeft() {
		return left;
	}

	public final void setLeft(final String left) {
		this.left = left;
	}

	public final String getValidTo() {
		return validTo;
	}

	public final void setValidTo(final String validTo) {
		this.validTo = validTo;
	}

	public final String getNumbValidTo() {
		return numbValidTo;
	}

	public final void setNumbValidTo(String numbValidTo) {
		this.numbValidTo = numbValidTo;
	}

}
