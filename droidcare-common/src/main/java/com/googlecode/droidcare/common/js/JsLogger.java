package com.googlecode.droidcare.common.js;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsLogger {
	
	private static final Logger log = LoggerFactory.getLogger(JsLogger.class);
	
	public void debug(String msg, Object arg1, Object arg2) {
		log.debug(msg, arg1, arg2);
	}
	
	public void info(String msg, Object arg1, Object arg2) {
		log.info(msg, arg1, arg2);
	}
	
	public void error(String msg, Object arg1, Object arg2) {
		log.error(msg, arg1, arg2);
	}

}
