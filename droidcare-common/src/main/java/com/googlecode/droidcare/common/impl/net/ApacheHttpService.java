package com.googlecode.droidcare.common.impl.net;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.googlecode.droidcare.common.api.net.HttpService;
import com.googlecode.droidcare.common.impl.exception.HttpError;
import com.googlecode.droidcare.common.net.Response;
import com.googlecode.droidcare.common.util.Utils;

public class ApacheHttpService implements HttpService {
	
	private static final String ENCODING = "UTF-8";

	private DefaultHttpClient defaulthttpclient;
	private BasicHttpContext basichttpcontext;
	private ArrayList<NameValuePair> params;
	
	public ApacheHttpService() {
		defaulthttpclient = new DefaultHttpClient();
		defaulthttpclient.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
		basichttpcontext = new BasicHttpContext();
		BasicCookieStore basiccookiestore = new BasicCookieStore();
		basichttpcontext.setAttribute("http.cookie-store", basiccookiestore);
		params = new ArrayList<NameValuePair>();
		
	}
	
	protected HttpResponse execute(HttpUriRequest request) {
		try {
			return defaulthttpclient.execute(request, basichttpcontext);
		} catch (ClientProtocolException e) {
			throw new HttpError("Failed to execute HTTP request", e);
		} catch (IOException e) {
			throw new HttpError("Failed to execute HTTP request", e);
		}
	}
	
	protected void clearParams() {
		params.clear();
	}
	
	@Override
	public void addParam(String name, String value) {
		params.add(new BasicNameValuePair(name, value));
	}
	
	protected HttpUriRequest createPostRequest(String url) {
		HttpPost req = new HttpPost(url);
		UrlEncodedFormEntity urlencodedformentity;
		try {
			urlencodedformentity = new UrlEncodedFormEntity(params, ENCODING);
			req.setEntity(urlencodedformentity);
		} catch (UnsupportedEncodingException e) {
			throw new HttpError("Error creating POST request", e);
		}
		return req;
	}
	
	protected HttpUriRequest createGetRequest(String url) {
		return new HttpGet(url);
	}
	
	protected Response wrapResponse(HttpResponse response) {
		checkResponse(response);
		Response r = new Response();
		r.setDom(parseDom(response.getEntity()));
		return r;
	}
	
	protected Document parseDom(HttpEntity entity) {
		Document dom = null;
		InputStream is = null;
		Header cType = entity.getContentType();
		if (!cType.getValue().contains("text/html")) {
			return dom;
		}
		try {
			is = entity.getContent();
			dom = Jsoup.parse(is, ENCODING, "");
			is.close();
		} catch (IllegalStateException e) {
			throw new HttpError("Failed to parse dom", e);
		} catch (IOException e) {
			throw new HttpError("Failed to parse dom", e);
		} finally {
			Utils.closeQuietly(is);
		}
		return dom;
	}
	
	protected void checkResponse(HttpResponse response) {
		if (response.getStatusLine().getStatusCode() >= 400) {
			throw new HttpError("Http connection error, server returned: " + response.getStatusLine());
		}
		if (response.getEntity() == null) {
			throw new HttpError("Http connection error, response contains no content");
		}
	}

	@Override
	public Response post(String url) {
		HttpResponse response = execute(createPostRequest(url));
		clearParams();
		return wrapResponse(response);
	}

	@Override
	public Response get(String url) {
		HttpResponse response = execute(createGetRequest(url));
		clearParams();
		return wrapResponse(response);
	}


}
