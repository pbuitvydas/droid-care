package com.googlecode.droidcare.common.view;

/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;

import com.googlecode.droidcare.common.model.Model;
import com.googlecode.droidcare.common.provider.SelfServiceProvider;
import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;

public class ViewRenderer {
	
	private Template template;
	
	public void loadTemplate(File file) throws FileNotFoundException {
		loadTemplate(new FileInputStream(file));
	}
	
	public void loadTemplate(InputStream is) {
		template = Mustache.compiler().compile(new InputStreamReader(is));
	}
	
	public void loadTemplate(Model model) {
		InputStream is = ViewRenderer.class.getClassLoader().getResourceAsStream(model.getView());
		loadTemplate(is);
	}
	
	public void renderTemplate(Model model, OutputStream os) {
		template.execute(model.getData(), new OutputStreamWriter(os));
	}
	
	public String renderTemplate(Model model) {
		return template.execute(model.getData());
	}
	
	public String renderView(SelfServiceProvider provider) {
		loadTemplate(provider.getModel());
		return renderTemplate(provider.getModel());
	}

}
