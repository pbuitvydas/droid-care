/*
 * #%L
 * droidcare-common
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
var dCare = dCare || {};
dCare.version = dCare.version || '${project.version}';
dCare.registerProvider = dCare.registerProvider || {};
dCare.http = dCare.http || {};
dCare.http.addParam = dCare.http.addParam || {};
dCare.http.get = dCare.http.get || {};
dCare.http.post = dCare.http.post || {}; 
dCare.logInfo = dCare.logInfo || {};
dCare.logDebug = dCare.logDebug || {};
dCare.logError = dCare.logError || {};
dCare.addError = dCare.addError || {};

dCare.registerProvider = function(providerInfo) {
	dCare.logInfo('Registering provider {}', providerInfo.id);
	var providerManager = Context.providerManager;
	var provider = Context.newProvider();
	var providerDescriptor = provider.providerDescriptor;
	
	providerDescriptor.name = providerInfo.name;
	providerDescriptor.version = providerInfo.version;
	providerDescriptor.description = providerInfo.description;
	
	provider.providerService.updateFunc = providerInfo.updateFunc;
	provider.model.view = providerInfo.view;
	providerManager.registerProvider(providerInfo.id, provider);
};

/**
* HTTP
*/
dCare.http.addParam = function(key, value) {
	Context.httpService.addParam(key, value);
};

dCare.http.get = function(url) {
	dCare.logInfo('HTTP GET to {}', url);
	return Context.httpService.get(url);
};

dCare.http.post = function(url) {
	dCare.logInfo('HTTP POST to {}', url);
	return Context.httpService.post(url);
};

/**
* Logging
*/
dCare.logInfo = function(msg, arg1, arg2) {
	Context.logger.info(msg, arg1, arg2);
};

dCare.logDebug = function(msg, arg1, arg2) {
	Context.logger.debug(msg, arg1, arg2);
};

dCare.logError = function(msg, arg1, arg2) {
	Context.logger.error(msg, arg1, arg2);
};

dCare.addError = function(msg) {
	Context.errorManager.throwError(msg);
};
