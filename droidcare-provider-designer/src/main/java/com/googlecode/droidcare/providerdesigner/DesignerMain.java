package com.googlecode.droidcare.providerdesigner;

/*
 * #%L
 * droidcare-provider-designer
 * %%
 * Copyright (C) 2012 - 2013 Paulius Buitvydas
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.tools.debugger.Main;
import org.mozilla.javascript.tools.shell.Global;

import com.googlecode.droidcare.common.AppContext;
import com.googlecode.droidcare.common.ScriptEngine;

public class DesignerMain extends Main {

	public DesignerMain(String title) {
		super(title);
	}

	public static void main(String[] args) {
		changeLaf();
        Main main = new Main("Droidcare JavaScript Debugger");
        main.doBreak();
        main.setExitAction(new Runnable() {
			
			public void run() {
				System.exit(0);
			}
		});

        System.setIn(main.getIn());
        System.setOut(main.getOut());
        System.setErr(main.getErr());

        Global global = org.mozilla.javascript.tools.shell.Main.getGlobal();
        AppContext.getInstance().setScriptEngine(new ScriptEngine(global));
        global.setIn(main.getIn());
        global.setOut(main.getOut());
        global.setErr(main.getErr());

        main.attachTo(
            org.mozilla.javascript.tools.shell.Main.shellContextFactory);

        main.setScope(global);

        main.pack();
        main.setSize(600, 460);
        main.setVisible(true);

        org.mozilla.javascript.tools.shell.Main.exec(args);
	}
	
	public static void changeLaf() {
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		}
	}
}
